/// Copyright (c) 2020 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
class FileDownloadViewController: UIViewController {

  
  
    @IBOutlet weak var FileUrlTextViewOutlet: UITextView!
    
    
    @IBOutlet weak var DownloadProgressOutlet: UIProgressView!
    
    @IBOutlet weak var DownloadProgressTxt: UILabel!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func DownloadBtnAction(_ sender: UIButton) {
        print("hello")
      var str : String
        str =  FileUrlTextViewOutlet.text;
      let furl = URL(string: str)
    //  fileDownloadService.startFileDownload(fileUrl: fUrl!)
      
      let track  = Track.init(name: "download", artist: "download", previewURL: furl!, index: 0)
      DownloadProgressOutlet.progress = 0;
       fileDownloadService.startDownload(track)
       
    }


  // MARK: - Constants
   //
   
   /// Get local file path: download task stores tune here; AV player plays it.
   let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("AllFiles")
 
   let fileDownloadService = FileDownloadService()
   let queryService = QueryService()


 // MARK: - Variables And Properties
  //
  lazy var fileDdownloadsSession: URLSession = {
    let configuration = URLSessionConfiguration.background(withIdentifier:
      "com.raywenderlich.pdfmaster.bgSession")
    return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
  }()
  
  var searchResults: [Track] = []
  
  
  lazy var tapRecognizer: UITapGestureRecognizer = {
    var recognizer = UITapGestureRecognizer(target:self.tableView, action: #selector(dismissKeyboard))
    return recognizer
  }()
  
  //
  // MARK: - Internal Methods
  //
  @objc func dismissKeyboard() {
    FileUrlTextViewOutlet.resignFirstResponder()
  }
  
  func localFilePath(for url: URL) -> URL {
    return documentsPath.appendingPathComponent(url.lastPathComponent)
  }
  
  
  
  
  
  
  func position(for bar: UIBarPositioning) -> UIBarPosition {
    return .topAttached
  }
  
  func reload(_ row: Int) {
    tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
  }
  
  //
  // MARK: - View Controller
  //
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.tableFooterView = UIView()
    fileDownloadService.fileDdownloadsSession = fileDdownloadsSession
    DownloadProgressOutlet.progress = 0;
  }
}

//
// MARK: - Search Bar Delegate
//
extension FileDownloadViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    dismissKeyboard()
    
    guard let searchText = searchBar.text, !searchText.isEmpty else {
      return
    }
    
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    
    queryService.getSearchResults(searchTerm: searchText) { [weak self] results, errorMessage in
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
      
      if let results = results {
        self?.searchResults = results
        self?.tableView.reloadData()
        self?.tableView.setContentOffset(CGPoint.zero, animated: false)
      }
      
      if !errorMessage.isEmpty {
        print("Search error: " + errorMessage)
      }
    }
  }
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    view.addGestureRecognizer(tapRecognizer)
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    view.removeGestureRecognizer(tapRecognizer)
  }
}

//
// MARK: - Table View Data Source
//
extension FileDownloadViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier:"ReuseCell",
                                                        for: indexPath)
    
    // Delegate cell button tap events to this view controller.
   // cell.delegate = self
    
    let track = searchResults[indexPath.row]
    cell.textLabel?.text = track.name
    cell.detailTextLabel?.text =  track.previewURL.absoluteString // as String;

    return cell
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return searchResults.count
  }
}

//
// MARK: - Table View Delegate
//
extension FileDownloadViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //When user taps cell, play the local file, if it's downloaded.
    
    let track = searchResults[indexPath.row]
    
    if track.downloaded {
     // playDownload(track)
    }
    
   // tableView.deselectRow(at: indexPath, animated: true)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 62.0
  }
}

//
// MARK: - Track Cell Delegate
//
extension FileDownloadViewController: TrackCellDelegate {
  func cancelTapped(_ cell: TrackCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      let track = searchResults[indexPath.row]
      fileDownloadService.cancelDownload(track)
      reload(indexPath.row)
    }
  }
  
  func downloadTapped(_ cell: TrackCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      let track = searchResults[indexPath.row]
      fileDownloadService.startDownload(track)
      reload(indexPath.row)
    }
  }
  
  func pauseTapped(_ cell: TrackCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      let track = searchResults[indexPath.row]
      fileDownloadService.pauseDownload(track)
      reload(indexPath.row)
    }
  }
  
  func resumeTapped(_ cell: TrackCell) {
    if let indexPath = tableView.indexPath(for: cell) {
      let track = searchResults[indexPath.row]
      fileDownloadService.resumeDownload(track)
      reload(indexPath.row)
    }
  }
}



 func createNewDirPath( )->URL{

 let dirPathNoScheme = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String

     //add directory path file Scheme;  some operations fail w/out it
     let dirPath = "file://\(dirPathNoScheme)"
     //name your file, make sure you get the ext right .mp3/.wav/.m4a/.mov/.whatever
     let fileName = "thisIsYourFileName.mov"
     let pathArray = [dirPath, fileName]
  let path = URL(string: pathArray[0])

     //use a guard since the result is an optional
     guard let filePath = path else {
         //if it fails do this stuff:
         return URL(string: "choose how to handle error here")!
     }
     //if it works return the filePath
     return filePath
 }


//
// MARK: - URL Session Delegate
//
extension FileDownloadViewController: URLSessionDelegate {
  func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
    DispatchQueue.main.async {
      if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
        let completionHandler = appDelegate.backgroundSessionCompletionHandler {
        appDelegate.backgroundSessionCompletionHandler = nil
        completionHandler()
      }
    }
  }
}

//
// MARK: - URL Session Download Delegate
//
extension FileDownloadViewController: URLSessionDownloadDelegate {
  func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                  didFinishDownloadingTo location: URL) {
    // 1
    guard let sourceURL = downloadTask.originalRequest?.url else {
      return
    }
   
    
  //  var da : NSData =   NSData(contentsOf: location, options: Data.ReadingOptions.Type)
    let download = fileDownloadService.activeDownloads[sourceURL]
    fileDownloadService.activeDownloads[sourceURL] = nil
    
    // 2
    let destinationURL = localFilePath(for: sourceURL)
    print(destinationURL)
    
    // 3
    let fileManager = FileManager.default
    try? fileManager.removeItem(at: destinationURL)
    
    do { //destinationURL
      try fileManager.copyItem(at: location, to: destinationURL)
      download?.track.downloaded = true
    } catch let error {
      print("Could not copy file to disk: \(error.localizedDescription)")
    }
    
    // 4
   // if let index = download?.track.index {
      DispatchQueue.main.async { [weak self] in
        let item : Track?
          item = download?.track
        self?.searchResults.append(item!)
        self?.tableView.reloadData()
        print(item?.previewURL);
       // self?.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        
      }
   // }
  }
  
  
  func updateProgressDisplay(progress: Float, totalSize : String) {
    DownloadProgressOutlet.progress = progress
    DownloadProgressTxt.text = String(format: "%.1f%% of %@", progress * 100, totalSize)
  }
  
  func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                  didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                  totalBytesExpectedToWrite: Int64) {
    // 1
    guard
      let url = downloadTask.originalRequest?.url,
      let download = fileDownloadService.activeDownloads[url]  else {
        return
    }
    print("total byte \(totalBytesExpectedToWrite)")
    // 2
    download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
    // 3
    let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite, countStyle: .file)
   // https://duet.ac.bd/wp-content/uploads/2016/08/CV-Format-for-recruitments.pdf
    // 4
    DispatchQueue.main.async {
      
      self.updateProgressDisplay(progress: download.progress, totalSize: totalSize)
      print("download end")
      /*
       if let trackCell = self.tableView.cellForRow(at: IndexPath(row: download.track.index,
                                                                 section: 0)) as? TrackCell {
         trackCell.updateDisplay(progress: download.progress, totalSize: totalSize)
      }
     */
    }
  }
}

 
